import "./globals.css";


export const metadata = {
  title: "FC News",
  description: "Portal de noticias FC",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className="bg-zinc-400">{children}</body>
    </html>
  );
}
